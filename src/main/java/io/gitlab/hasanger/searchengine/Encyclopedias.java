package io.gitlab.hasanger.searchengine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Encyclopedias {
	
	// Logger for the class
	static Logger logger = LoggerFactory.getLogger(Encyclopedias.class);
	
	// List of encyclopedias
	public static ArrayList<Encyclopedia> encyclopedias = new ArrayList<Encyclopedia>();
	
	// HeadlessBrowser instance. HeadlessBrowser is a custom class I created specifically for this project.
	static HeadlessBrowser browser = new HeadlessBrowser();
	
	// Static initializer. Loads all encyclopedias and puts them in the encyclopedias array
	static {
		try {
			// Load the file
			JSONObject encyclopediasObject = new JSONObject(new String(Files.readAllBytes(new File("encyclopedias.json").toPath())));
			
			// Get the JSONArray of encyclopedias and convert it to an ArrayList
			JSONArray encyclopediasArray = encyclopediasObject.getJSONArray("encyclopedias");
			for(int i = 0; i < encyclopediasArray.length(); i++) {
				encyclopedias.add(new Encyclopedia((JSONObject) encyclopediasArray.get(i)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// List of articles, used exclusively by the searchAll method.
	// Has to be outside of the method because this variable is used in lambdas.
	static ArrayList<ArticleData> dataList = new ArrayList<>();
	
	// Jsoup Document object, also used exclusively by the searchAll method.
	// Is outside of the method for the same reason as dataList.
	static Document document;
	
	/**
	 * Search all encyclopedias.
	 * @param query What to search for
	 * @param numResults The number of results to load from each encyclopedia
	*/
	public static ArrayList<ArticleData> searchAll(String query, int numResults) {
		
		// Clear list of results
		dataList.clear();
		
		// Make a list of threads, so we can block until all threads are done executing
		ArrayList<Thread> threads = new ArrayList<>();
		
		CountDownLatch threadLatch = new CountDownLatch(encyclopedias.size());
		
		// Loop over all encyclopedias
		for(Encyclopedia encyclopedia : encyclopedias) {
			
			// Create a new thread for every encyclopedia, querying each simultaneously to save time
			Thread t = new Thread(() -> {
				
				// Get the search data using Jsoup, or a headless browser if necessary
				
				long start = System.currentTimeMillis();
				
				String searchUrl = encyclopedia.searchUrl.replace("QUERY", query);
				
				logger.info("Loading " + encyclopedia.name + " page...");
				
				// Check if the encyclopedia loads results with JS,
				// and retrieve the content different ways depending on this
				if(encyclopedia.requiresPageLoading) {
					
					// If the page requires loading (i.e. the results are loaded by JS):
					browser.navigateAndWait(searchUrl); // Load the HTML. The navigateAndWait() method blocks until the page is loaded
					document = Jsoup.parse(browser.getHTML()); // Parse the loaded HTML
					
				} else {
					
					// Otherwise, just retrieve the page without loading
					try {
						document = Jsoup.connect(searchUrl).get();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				long end = System.currentTimeMillis();
				float sec = (end - start) / 1000F;
				logger.info("Done loading " + encyclopedia.name + " page. Took " + sec + "s"); // Log
				
				// For every result...
				for(int i = 0; i < (encyclopedia.duplicateResultFix ? numResults+1 : numResults); i++) {
					
					// Scrape the website and add the results to the array
					try {
						ArticleData data = encyclopedia.search(document, query, i);
						if(data != null) dataList.add(data);
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
				threadLatch.countDown();
			});
			threads.add(t); // Add it to the array
		}
		
		// Block until all threads are finished executing,
		// so that the array isn't sorted and returned before it's done
		for(Thread t : threads) t.start();
		try {
			threadLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// Remove duplicate results. These appear sometimes for some reason
		dataList = removeDuplicates(dataList);
		
		// Sort data twice: once by relevance (see Encyclopedia class),
		// and once by Levenshtein distance (how similar the article titles are to the query)
		
		// First sort: by relevance
		Collections.sort(dataList, (a1, a2) -> {
			return a2.relevance - a1.relevance;
		});
		
		// Second sort: by Levenshtein distance
		Collections.sort(dataList, (a1, a2) -> {
			return a1.levenshteinRelevance - a2.levenshteinRelevance;
		});
		
		// Return the results
		return dataList;
	}
	
	// Function to remove duplicates from an ArrayList
	// https://www.geeksforgeeks.org/how-to-remove-duplicates-from-arraylist-in-java/
    public static ArrayList<ArticleData> removeDuplicates(ArrayList<ArticleData> list) { 
  
        // Create a new ArrayList 
        ArrayList<ArticleData> newList = new ArrayList<ArticleData>();
  
        // Traverse through the first list 
        for(ArticleData element : list) {
        	
        	boolean containsElement = false;
        	
        	for(ArticleData newElement : newList) {
        		if(newElement.articleTitle.equals(element.articleTitle)) containsElement = true;
        	}
        	
        	if(!containsElement) newList.add(element);
        } 
  
        // return the new list 
        return newList; 
    }

}
