package io.gitlab.hasanger.searchengine;

import static spark.Spark.*;

import java.io.File;
import java.nio.file.Files;

import org.json.JSONObject;
import org.json.JSONStringer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * The SearchEngineServer class is the main class.
*/
public class SearchEngineServer {
	
	// Logger
	static Logger logger = LoggerFactory.getLogger(SearchEngineServer.class);
	
	// The number of results to load for each encyclopedia.
	// Can't be in the main() method because it's used in a lambda.
	static int numResults = 5;
	
	public static void main(String[] args) {
		
		// Get configuration
		String ipAddress = "0.0.0.0";
		int port = 8080;
		try {
			JSONObject config = new JSONObject(new String(Files.readAllBytes(new File("config.json").toPath())));
			ipAddress = config.getString("ipAddress");
			port = config.getInt("port");
			numResults = config.getInt("numResults");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Set up server
		
		// Set IP address and port based on config.json
		ipAddress(ipAddress);
		port(port);
		
		// Set up static files
		staticFiles.location("/public");
		
		
		// GET request for search
		get("/search/:query", (req, res) -> {
			
			logger.info("Searching for \"" + req.params("query") + "\"..."); // Log message
			
			res.type("application/json"); // Let the browser know that this is JSON
			
			// Instantiate JSONStringer for results
			JSONStringer stringer = new JSONStringer();
			stringer.object()
					.key("searchResults").array();
			
			// Do the actual search, and add the results to the JSON object
			for(ArticleData data : Encyclopedias.searchAll(req.params("query"), numResults)) {
				stringer.object()
						.key("encyclopediaName").value(data.encyclopediaName)
						.key("encyclopediaImageName").value(data.encyclopediaImageName)
						.key("articleTitle").value(data.articleTitle)
						.key("shortDescription").value(data.shortDescription)
						.key("searchUrl").value(data.searchUrl)
						.key("url").value(data.url)
						.endObject();
			}
			stringer.endArray()
					.endObject();
			
			// Return the results
			return stringer.toString();
		});
		
	}

}