$(document).ready(function() {
	$('#searchBox').keypress(function(e){
      if(e.keyCode==13) $('#searchBtn').click();
    });
	$('.collapsible').collapsible();
	$(".wrapper-wrapper, .collection").hide();
	$("#searchBtn").click(function() {
		search($("#searchBox").val());
	});
});

function search(query) {
	if(!query.trim() == "") {
		$(".wrapper-wrapper").show();
		$(".collection").hide().html("");
		$.get("/search/" + query, function(results) {
			console.log(results.searchResults);
			for(var i = 0; i < results.searchResults.length; i++) {
				var result = results.searchResults[i];
				newItem = $('<li class="collection-item avatar" />');
				newItem.append($('<img src="img/' + result.encyclopediaImageName + '.png" alt="' + result.encyclopediaName + '" class="circle" style="border-radius: 0px;">'));
				newItem.append('<a class="title" href="' + result.url + '" target="_blank">' + result.articleTitle + '</a>');
				newItem.append('<p>' + result.shortDescription + '</p>');
				newItem.append('<a href="' + result.searchUrl + '" class="secondary-content valign-wrapper" target="_blank">See more at ' + result.encyclopediaName + '<i class="material-icons right">open_in_new</i></a>');
				$(".collection").append(newItem);
				/*newItem = $('<li />');
				newItem.append($('<div class="collapsible-header"><img src="img/' + result.encyclopediaImageName + '.png" alt="' + result.encyclopediaName + '" class="circle"><strong class="title">' + result.articleTitle + '</strong><a href="' + result.searchUrl + '" class="secondary-content valign-wrapper">See more at ' + result.encyclopediaName + '<i class="material-icons right">open_in_new</i></a></div>'));
				newItem.append('<div class="collapsible-body"><p>' + result.shortDescription + '</p></div>');
				$(".collapsible").append(newItem);*/
			}
			$(".wrapper-wrapper").hide();
			$(".collection").show();
		});
	}
}