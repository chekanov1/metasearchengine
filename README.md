# MetaSearchEngine

MetaSearchEngine is an experimental project written in Java. It allows you to search a number of encyclopedias at once.

## Installation
### Windows
```
git clone https://gitlab.com/ks_found/MetaSearchEngine
cd MetaSearchEngine
gradlew run
```

### Mac/Linux
```
git clone https://gitlab.com/ks_found/MetaSearchEngine
cd MetaSearchEngine
./gradlew run
```

### Detailed instructions

1. Make sure you have a modern version of Java installed (run `java -version` to check).

2. Run `git clone https://gitlab.com/ks_found/MetaSearchEngine`.

   Alternatively, if you don't have Git installed, you can [download the repo from the project page](https://gitlab.com/ks_found/metasearchengine/-/archive/master/metasearchengine-master.zip) and extract.

3. If you downloaded the repo using the link, open a terminal in the `MetaSearchEngine` directory.

   If you cloned it using Git, type `cd MetaSearchEngine` in the same terminal.

4. Start the server.

    Windows: `gradlew run`

    Mac/Linux: `./gradlew run`

5. The server should start in a few seconds. After starting, go to `0.0.0.0:8080` (default).

6. Optionally, settings like the IP address and port can be changed in `config.json`.

## Contribution
Any contributions are welcome! See [CONTRIBUTING.md](CONTRIBUTING.md) for details.